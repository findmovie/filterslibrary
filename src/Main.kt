fun main() {
    val data = listOf(1.0, 3.0, 5.0, 6.0, 8.0, 12.0, 18.0, 21.0, 22.0, 25.0)
    val smoothingNoisy = SmoothingNoisy(data)

    println(smoothingNoisy.getSimpleMovingAverage(3))
    println(smoothingNoisy.getExponentialMovingAverage(0.5))
    println(smoothingNoisy.computeKalman())
}
