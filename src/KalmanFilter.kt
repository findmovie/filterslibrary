private const val Q = 0.000001 // processVariance
private const val R = 0.001 // estimationVariance

class KalmanFilter {

    /**
     * @param values - список значений типа Double, которые характеризуют временной ряд
     *
     * @return - список усредненных значений типа Double
     */
    fun filter(values: MutableList<Double>): List<Double> {
        val numOfMeasurements = values.size
        val z = DoubleArray(numOfMeasurements)

        // Estimation variance
        val xPredict = DoubleArray(numOfMeasurements) // estimated true value (posteri)
        val xCurrent = DoubleArray(numOfMeasurements) // estimated true value (priori)
        val Ppredict = DoubleArray(numOfMeasurements) // estimated error (posteri)
        val Pcurrent = DoubleArray(numOfMeasurements) // estimated error (priori)
        val k = DoubleArray(numOfMeasurements) // kalman gain

        // Initial guesses
        xPredict[0] = 0.0
        Ppredict[0] = 1.0

        for (i in 0 until numOfMeasurements) {
            z[i] = values[i]
        }

        if (values.size < 2) {
            return values
        }

        for (i in 1 until numOfMeasurements) {
            // time update
            xCurrent[i] = xPredict[i - 1]
            Pcurrent[i] = Ppredict[i - 1] + Q

            // measurement update
            k[i] = Pcurrent[i] / (Pcurrent[i] + R)
            xPredict[i] = xCurrent[i] + k[i] * (z[i] - xCurrent[i])
            Ppredict[i] = (1 - k[i]) * Pcurrent[i]
        }

        for (i in 0 until numOfMeasurements) {
            values[i] = xPredict[i]
        }

        values.removeAt(0)

        return values
    }
}
