import java.util.LinkedList

/**
 * @param period - сглаживающий интервал:
 * изменяя данный параметр можно получить большую или меньшую степень сглаживания данных
 */
class SimpleMovingAverage(private val period: Int) {

    private val buffer = LinkedList<Double>()
    private val data = LinkedList<Double>()

    /**
     * @param value - значение типа Double, которое является одной из точек временного ряда
     */
    fun addValue(value: Double) {
        data.add(value)
    }

    /**
     * @param values - список значений типа Double, которые характеризуют временной ряд
     */
    fun addValues(values: List<Double>) {
        data.clear()
        data.addAll(values)
    }

    /**
     * @return - список усредненных значений типа Double
     */
    fun getAverage(): List<Double> {
        val outputData = ArrayList<Double>()
        var sum = 0.0

        data.forEach { value ->
            sum += value
            buffer.add(value)

            if (buffer.size > period) {
                sum -= buffer.remove()
            }

            outputData.add(sum / period)
        }
        buffer.clear()

        return outputData
    }
}
