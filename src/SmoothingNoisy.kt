class SmoothingNoisy(private val values: List<Double>) {

    fun getSimpleMovingAverage(period: Int): List<Double> {
        val simpleMovingAverage = SimpleMovingAverage(period).apply {
            addValues(values)
        }

        return simpleMovingAverage.getAverage()
    }

    fun getExponentialMovingAverage(alpha: Double): List<Double> {
        val exponentialMovingAverage = ExponentialMovingAverage()

        return exponentialMovingAverage.getAverage(values, alpha)
    }

    fun computeKalman(): List<Double> {
        val kalmanFilter = KalmanFilter()

        return kalmanFilter.filter(values.toMutableList())
    }
}
