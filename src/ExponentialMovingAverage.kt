class ExponentialMovingAverage {

    /**
     * @param alpha - double от 0 до 1 - отражающий скорость старения прошлых данных:
     * чем выше его значение, тем больший удельный вес имеют новые наблюдения случайной величины, и тем меньший старые
     *
     * @return - список усредненных значений типа Double
     */
    fun getAverage(values: List<Double>, alpha: Double): List<Double> {
        val output = ArrayList<Double>()
        var oldEMA = 0.0

        for (i in values.indices) {
            if (i == 0) {
                oldEMA = values[i]
                output.add(oldEMA)
            }
            oldEMA = alpha * values[i] + (1 - alpha) * oldEMA
            output.add(oldEMA)
        }

        return output
    }
}
